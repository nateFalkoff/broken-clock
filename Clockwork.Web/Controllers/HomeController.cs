﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Http.Cors;
using Clockwork.Web.Models;

namespace Clockwork.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var mvcName = typeof(Controller).Assembly.GetName();
            var isMono = Type.GetType("Mono.Runtime") != null;

            TimeZoneModel model = new TimeZoneModel();
            model.TimeZones = GetTimeZones();

            ViewData["Version"] = mvcName.Version.Major + "." + mvcName.Version.Minor;
            ViewData["Runtime"] = isMono ? "Mono" : ".NET";
           
            return View(model);
        }

        private List<SelectListItem> GetTimeZones()
        {
            List<TimeZoneInfo> allTimeZones = TimeZoneInfo.GetSystemTimeZones().ToList();
            List<SelectListItem> timeZones = new List<SelectListItem>();
            foreach (TimeZoneInfo t in allTimeZones)
            {
                var item = new SelectListItem()
                {
                    Value = t.Id,
                    Text = t.DisplayName,
                    Selected = false,
                };
                timeZones.Add(item);
            }
            return timeZones;
        }
    }
}
