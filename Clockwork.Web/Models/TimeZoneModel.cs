﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Clockwork.Web.Models
{
    public class TimeZoneModel
    {
        public TimeZoneInfo SelectedTimeZoneInfo { get; set; }
        public List<SelectListItem> TimeZones { get; set; }
    }
}