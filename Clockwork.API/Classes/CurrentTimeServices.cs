﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Clockwork.API.Classes
{
    public class CurrentTimeServices
    {
        private List<string> AllTimeZoneIds =
        new List<string>(TimeZoneInfo.GetSystemTimeZones().Select(tz => tz.Id));

        public DateTime ConvertFromUtcIfValid(string timeZoneId, DateTime utcTime)
        {
            if (AllTimeZoneIds.Contains(timeZoneId))
            {
                TimeZoneInfo newZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                return TimeZoneInfo.ConvertTimeFromUtc(utcTime, newZone);
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Format("No Time Zone with ID = {0}", timeZoneId)),
                    ReasonPhrase = "Time Zone ID Not Found"
                };

                throw new System.Web.Http.HttpResponseException(resp);
            }

        }

    }
}
