﻿using System;
using Clockwork.API.Classes;
using Microsoft.EntityFrameworkCore;

namespace Clockwork.API.Models
{
    public class ClockworkContext : DbContext
    {

        public DbContext Instance => this;
        public virtual DbSet<CurrentTimeQuery> CurrentTimeQueries { get; set; }

        public ClockworkContext(DbContextOptions<ClockworkContext> options)
            : base(options) { }
    }

    public class CurrentTimeQuery
    {
        public int CurrentTimeQueryId { get; set; }
        public DateTime Time { get; set; }
        public string ClientIp { get; set; }
        public DateTime UTCTime { get; set; }
        public string TimeZone { get; set; }
    }
}
