﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using System.Collections.Generic;
using System.Linq;
using Clockwork.API.Classes;

namespace Clockwork.API.Controllers
{
    public class CurrentTimeController : Controller
    {
        private CurrentTimeServices TimeService = new CurrentTimeServices();
        private ClockworkContext _db;

        public CurrentTimeController(ClockworkContext db)
        {
            _db = db;
        }

        // GET api/currenttime
        [HttpPost]
        [Route("api/[controller]")]
        public IActionResult Post([FromBody] CurrentTimeQuery query)
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            var ip = this.HttpContext != null ? this.HttpContext.Connection.RemoteIpAddress.ToString() : "1";

            var returnVal = new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime,
                TimeZone = query != null && query.TimeZone != null ? query.TimeZone : TimeZoneInfo.Local.Id
            };

            DateTime convertedTime = TimeService.ConvertFromUtcIfValid(returnVal.TimeZone, returnVal.UTCTime);
            returnVal.Time = convertedTime;

            using (_db)
            {
                _db.CurrentTimeQueries.Add(returnVal);
                _db.SaveChanges();
            }

            return Ok(returnVal);
        }

        [HttpGet]
        [Route("api/[controller]/getall")]
        public IActionResult GetAll()
        {
            List<CurrentTimeQuery> returnVal = new List<CurrentTimeQuery>();

            using (_db)
            {
                returnVal = _db.CurrentTimeQueries.ToList();
            }

            return Ok(returnVal);
        }
    }
}
