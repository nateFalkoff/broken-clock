# README #

### Technologies Used

Bootstrap, Jquery

# Requirements 

### 1. Responsiveness ###

Responsive design was added with Bootstrap.

### 2. Display All Database Entries ###

A new GET method was added to the API to get all Time Queries.

The front-end method GetEntries handles making the API call and displaying the information on the page. GetEntries is called both when the page initially loads/is refreshed,
and also when the "Get Current Time" button is clicked.

I chose to display the data in a table. The data is unformatted. A new row in the database is automatically displayed, as the table is generated programatically with no hardcoded
headers or values.

### 3. Add TimeZone Awareness ###

A list of all possible TimeZones is generated in the HomeController. Those time zones populate a TimeZoneModel, 
which is passed to the view and used to make a dropdown menu.  The dropdown menu was made searchable by using Bootstrap Select.

The value of the dropdown menu is passed in a POST request. The API layer looks for a timeZone property.  If the property is not found, it defaults to Eastern Time Zone.
If an invalid time zone is sent, an exception is thrown.  If the timeZone is valid, it is saved to the database along with the rest of the query.

The Time property of the response is converted from UTC time using the new time zone.  The response is sent, and displayed as usual on the front end.

### Known Areas of Improvement ###

The biggest area of improvement I am aware of is automated tests. I found I wasn't sure of all the nuances of testing with Entity Framework and the Web API.  Should I be using mocks?
Should I be using an interface to inject the database context?  Should I be using an in-memory DB to test, or the production DB my API is using? How do I 
do full integration tests on an API? My knowledge of all these areas was fuzzy.

I researched some solutions and opinions of these issues, but ultimately decided to send the exercise knowing the tests are imperfect. I would have liked to
have full integration tests that actually send HttpRequests rather then unit testing the controller actions.

As this is an exercise to show what you know and how you think, I'd like to demonstrate that I know the importance of tests and writing testable code and that I did consider that.  

I'd also like to show I know the value of admitting I don't know something, so I can get help and learn it the right way from someone who has that knowledge. 

