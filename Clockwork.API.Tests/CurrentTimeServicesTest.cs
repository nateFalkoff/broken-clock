using NUnit.Framework;
using Moq;
using Microsoft.EntityFrameworkCore;
using Clockwork.API.Models;
using Clockwork.API.Controllers;
using System.Net.Http;
using System.Web.Http;
using Clockwork.API.Classes;
using System;

namespace Tests
{
    public class CurrentTimeServicesTest
    {
        CurrentTimeServices timeService;
        DateTime utc;

        [SetUp]
        public void Setup()
        {
            timeService = new CurrentTimeServices();
            utc = DateTime.UtcNow;
        }

        [Test]
        public void MockTest()
        {
            //Arrange 
            var mockSet = new Mock<DbSet<CurrentTimeQuery>>();

            //Act
            var mockContext = new Mock<ClockworkContext>();
            mockContext.Setup(m => m.CurrentTimeQueries).Returns(mockSet.Object);
            mockSet.Object.Add(new CurrentTimeQuery());

            //Assert
            mockSet.Verify(m => m.Add(It.IsAny<CurrentTimeQuery>()), Times.Once());
        }

        [Test]
        public void ValidTimeZoneIDReturnsConvertedTime()
        {
            //Arrange
            string zoneId = "Eastern Standard Time";

            //Act
            DateTime result = timeService.ConvertFromUtcIfValid(zoneId, utc);

            Assert.AreEqual(utc.AddHours(-4), result);
        }

        [Test]
        public void InvalidTimeZoneIDReturnsException()
        {
            //Arrange
            string zoneId = "Not a real zone id";

            //Act // Assert
            Assert.Throws<HttpResponseException>(() => timeService.ConvertFromUtcIfValid(zoneId, utc));

        }
    }
}