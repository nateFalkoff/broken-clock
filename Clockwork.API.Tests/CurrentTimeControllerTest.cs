﻿using NUnit.Framework;
using Microsoft.EntityFrameworkCore;
using Clockwork.API.Models;
using Clockwork.API.Controllers;
using System.Web.Http;
using Clockwork.API.Classes;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;


namespace Tests
{
    public class CurrentTimeControllerTest
    {
        CurrentTimeController controller;
        ClockworkContext db;


       [SetUp]
        public void Setup()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ClockworkContext>();
            optionsBuilder.UseInMemoryDatabase(databaseName: "Test");
            db = new ClockworkContext(optionsBuilder.Options);
            controller = new CurrentTimeController(db);
            Seed(db);
        }

        [Test]
        public void GetAllReturnsOkStatusWithAllDbEntries()
        {
            //Arrange


            //Act
            var result = controller.GetAll();
            var okResult = result as OkObjectResult;
            List<CurrentTimeQuery> list = okResult.Value as List<CurrentTimeQuery>;

            //Assert
            Assert.IsNotNull(okResult);
            Assert.AreEqual(200, okResult.StatusCode);
            Assert.AreEqual(2, list.Count);
        }

        [Test]
        public void PostWithValidTimeIdReturnsConvertedTime()
        {
            //Arrange
            var request = new CurrentTimeQuery
            {
                UTCTime = DateTime.UtcNow,
                ClientIp = "44",
                Time = DateTime.Now,
                TimeZone = "Central Standard Time"
            };
            //Act
            var response = controller.Post(request);
            var okResult = response as OkObjectResult;
            CurrentTimeQuery result = okResult.Value as CurrentTimeQuery;

            //Assert
            Assert.IsNotNull(okResult);
            Assert.AreEqual(200, okResult.StatusCode);
            Assert.AreEqual("Central Standard Time", result.TimeZone);
            Assert.AreEqual(result.UTCTime.AddHours(-5), result.Time);
        }

        [Test]
        public void PostWithInValidTimeIdReturns400StatusCode()
        {
            //Arrange
            var request = new CurrentTimeQuery
            {
                UTCTime = DateTime.UtcNow,
                ClientIp = "44",
                Time = DateTime.Now,
                TimeZone = "NOT A VALID TIMEZONE"
            };

            //Act
            try
            {
                var response = controller.Post(request);
                var okResult = response as OkObjectResult;
            }
            //Assert
            catch (HttpResponseException e)
            {
                Assert.AreEqual("BadRequest", e.Response.StatusCode.ToString());
            }
        }

        private void Seed(ClockworkContext context)
        {
            var currentTimeQueries = new[]
            {
              new CurrentTimeQuery
                {
                UTCTime = DateTime.UtcNow,
                ClientIp = "44",
                Time = DateTime.Now,
                TimeZone = "Eastern Standard Time"
                },
              new CurrentTimeQuery
                {
                UTCTime = DateTime.UtcNow,
                ClientIp = "44",
                Time = DateTime.Now,
                TimeZone = "Central Standard Time"
                }
            };

            context.CurrentTimeQueries.AddRange(currentTimeQueries);
            context.SaveChanges();
        }
    }
}